CC=gcc
CFLAGS=-Wall -O3 -pipe
BINARY=dni

${BINARY}: ${BINARY}.c

clean:
	rm -f ${BINARY}
