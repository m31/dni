#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "lang/spanish.h"

int randomBetween(int min, int max) { return min + rand() % ( max - min + 1 ); }
char calcDniLetter(int num) { return "TRWAGMYFPDXBNJZSQVHLCKE"[num % 23]; }
void printDniWithLetter(int num) { printf("%08d%c\n", num, calcDniLetter(num)); }

int main(int argc, char *argv[])
{
	switch(getopt(argc, argv, "n:hv"))
	{
		case 'n':

			printDniWithLetter(atoi(optarg));
			break;

		case 'h':

			printf(STR_PROGRAM_HELP);
			break;

		case 'v':

			printf("DNI Version 0.3\n");
			break;

		default:

			srand(time(NULL) - getpid());
			if(time(NULL) < getpid()) { printf(STR_TIME_LT_GETPID); return 1; }
			printDniWithLetter(randomBetween(0,99999999));
	}

	return 0;
}
