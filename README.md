### Características

* Util para usarlo en scripts de shell
* Calcular la letra de comprobación
* Generar número de DNI aleatorio

### Idiomas

Para cambiar el idioma, edita en main.c la siguiente línea:

#include "lang/spanish.h"

Y cámbiala por esta otra:

#include "lang/klingon.h"

### Bugs

* [ARREGLADO] No se imprimían los ceros a la derecha del número
del DNI si éste los tenía.
